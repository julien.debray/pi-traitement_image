from typing import List
import numpy as np
from PIL import Image
import numpy.typing as npt
from matplotlib import pyplot as plt
import cv2
import math
import sys

Img = npt.NDArray[np.uint8]
HoughAcc = npt.NDArray[np.uint8]
Line = tuple

def load_img(path: str) -> Img:
  global gris
  with Image.open(path) as im:
    im.load()
    if(im.mode != 'RGB'):
      gris = True
    return np.asarray(im)          
        
def show_img(img: Img, axis: plt.axes) -> None:
  axis.imshow(img, cmap='gray')

def pretraitement(img: Img) -> Img:
  global gris
  new_img = cv2.GaussianBlur(img, (5, 5), 3)
  if(not gris):
    new_img = cv2.cvtColor(new_img, cv2.COLOR_BGR2GRAY)
  new_img = cv2.Laplacian(new_img, cv2.CV_8U, ksize=5)
  new_img = cv2.blur(new_img, (3, 3))
  new_img = seuillage(new_img, 184)
  return new_img

def seuillage(img: Img, seuil: int) -> Img:
  for i in range(img.shape[0]):
    for j in range(img.shape[1]):
      if img[i][j] < seuil:
        img[i][j] = 0
      else:
        img[i][j] = 255
  return img

def hough_space(img: Img, angle_step: int = 1) -> HoughAcc:
  x, y = img.shape
  accum = np.zeros((math.floor(math.sqrt(x ** 2 + y ** 2)), 360))

  for i in range(x):
    for j in range(y):
      if img[i][j] != 0:
        for theta in range(0, 360, angle_step):
          rho = int(j * math.cos(math.radians(theta)) + i * math.sin(math.radians(theta)))
          if rho > 0 and rho < math.floor(math.sqrt(x ** 2 + y ** 2)):
            accum[rho][theta] += 1
  return accum

def hough_lines(acc: HoughAcc) -> List[Line]:
  list = []
  for i in range(acc.shape[0]):
    for j in range(acc.shape[1]):
      if acc[i][j] >= 400: # 280
        list.append((i, j))
  return list

def draw_lines(img: Img, lines: List[Line]) -> Img:
  new_img = img.copy()
  for rho, theta in lines:
    m = math.cos(math.radians(theta))
    c = math.sin(math.radians(theta))
    x0 = int(m * rho - 1000 * (-c))
    y0 = int(c * rho - 1000 * m)
    x1 = int(m * rho + 1000 * (-c))
    y1 = int(c * rho + 1000 * m)
    cv2.line(new_img, (x0, y0), (x1, y1), (255, 0, 0), 1, cv2.LINE_AA)
  return new_img

gris = False

img = load_img(sys.argv[1])

contour_img = pretraitement(img)
accumulator = hough_space(contour_img)
lines = hough_lines(accumulator)
img_with_lines = draw_lines(img, lines)

# _, axes = plt.subplots(2, 2)
# show_img(img, axes[0][0])
# show_img(contour_img, axes[0][1])
# show_img(accumulator, axes[1][0])
# show_img(img_with_lines, axes[1][1])
# plt.show()
cv2.imshow('image', img_with_lines)
cv2.waitKey(0) 
cv2.destroyAllWindows()
