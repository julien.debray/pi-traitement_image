import cv2  # type: ignore
import numpy as np # type: ignore
from math import atan2, cos, sin, sqrt, pi
import sys

def drawAxis(img, p_, q_, color, scale):
  p = list(p_)
  q = list(q_)
  angle = atan2(p[1] - q[1], p[0] - q[0]) # angle in radians
  hypotenuse = sqrt((p[1] - q[1]) * (p[1] - q[1]) + (p[0] - q[0]) * (p[0] - q[0]))
  q[0] = p[0] - scale * hypotenuse * cos(angle)
  q[1] = p[1] - scale * hypotenuse * sin(angle)
  cv2.line(img, (int(p[0]), int(p[1])), (int(q[0]), int(q[1])), color, 3, cv2.LINE_AA)
  p[0] = q[0] + 9 * cos(angle + pi / 4)
  p[1] = q[1] + 9 * sin(angle + pi / 4)
  cv2.line(img, (int(p[0]), int(p[1])), (int(q[0]), int(q[1])), color, 3, cv2.LINE_AA)
  p[0] = q[0] + 9 * cos(angle - pi / 4)
  p[1] = q[1] + 9 * sin(angle - pi / 4)
  cv2.line(img, (int(p[0]), int(p[1])), (int(q[0]), int(q[1])), color, 3, cv2.LINE_AA)

def getOrientation(pts, img):
  sz = len(pts)
  data_pts = np.empty((sz, 2), dtype=np.float64)
  for i in range(data_pts.shape[0]):
    data_pts[i,0] = pts[i,0,0]
    data_pts[i,1] = pts[i,0,1]
  mean = np.empty((0))
  mean, eigenvectors, eigenvalues = cv2.PCACompute2(data_pts, mean)
  cntr = (int(mean[0,0]), int(mean[0,1]))
  cv2.circle(img, cntr, 3, (255, 0, 255), 2)
  p1 = (cntr[0] + 0.02 * eigenvectors[0,0] * eigenvalues[0,0], cntr[1] + 0.02 * eigenvectors[0,1] * eigenvalues[0,0])
  p2 = (cntr[0] - 0.02 * eigenvectors[1,0] * eigenvalues[1,0], cntr[1] - 0.02 * eigenvectors[1,1] * eigenvalues[1,0])
  drawAxis(img, cntr, p1, (255, 255, 0), 1)
  drawAxis(img, cntr, p2, (0, 0, 255), 5)
  angle = atan2(eigenvectors[0,1], eigenvectors[0,0]) # orientation in radians
  label = "  Rotation Angle: " + str(round(-float(np.rad2deg(angle)), 2) - 90) + " degrees"
  cv2.rectangle(img, (cntr[0], cntr[1]-25), (cntr[0] + 300, cntr[1] + 10), (255,255,255), -1)
  cv2.putText(img, label, (cntr[0], cntr[1]), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,0,0), 1, cv2.LINE_AA)
 
  return angle
  
img = cv2.imread(sys.argv[1])
   
t_lower = 0
t_upper = 80

edge = cv2.Canny(img, t_lower, t_upper) 
cdst = cv2.cvtColor(edge, cv2.COLOR_GRAY2BGR)
cdstP = np.copy(cdst)

gray = cv2.cvtColor(cdst, cv2.COLOR_BGR2GRAY)
_, bw = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
contours, _ = cv2.findContours(bw, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
for i, c in enumerate(contours):
  area = cv2.contourArea(c)
  if area < 690 or 700 < area:
    continue
  # cv2.drawContours(img, contours, i, (0, 0, 255), 2)
  getOrientation(c, img)

# lines = cv2.HoughLines(edge, 1, np.pi / 180, 300, None, 0, 0)
lines = cv2.HoughLines(edge, 1, np.pi / 180, 150, None, 0, 0)

if lines is not None:
  for i in range(0, len(lines)):
    rho = lines[i][0][0]
    theta = lines[i][0][1]
    a = cos(theta)
    b = sin(theta)
    x0 = a * rho
    y0 = b * rho
    pt1 = (int(x0 + 1000*(-b)), int(y0 + 1000*(a)))
    pt2 = (int(x0 - 1000*(-b)), int(y0 - 1000*(a)))
    cv2.line(cdst, pt1, pt2, (0,0,255), 3, cv2.LINE_AA)

linesP = cv2.HoughLinesP(edge, 1, np.pi / 180, 50, None, 200, 10)

if linesP is not None:
  for i in range(0, len(linesP)):
    l = linesP[i][0]
    x1, y1, x2, y2 = l[0], l[1], l[2], l[3]
    line_length = sqrt((x2 - x1)**2 + (y2 - y1)**2)
    cv2.line(cdstP, (l[0], l[1]), (l[2], l[3]), (0,0,255), 3, cv2.LINE_AA)
    

cv2.imshow("Source", img)
# cv2.imshow("Detected Lines (in red) - Probabilistic Line Transform", cdstP)
# cv2.imshow("Detected Lines (in red) - Standard Hough Line Transform", cdst)

cv2.waitKey(0) 
cv2.destroyAllWindows()