import cv2  # type: ignore
import numpy as np # type: ignore
import os
import random
import pickle
import tensorflow as tf
from sklearn.preprocessing import LabelEncoder
from tensorflow.keras.preprocessing.image import ImageDataGenerator

def create_training_data():
    training_data = []
    dataPath = "../img"
    labels = open("../data/labels", "r").readline().split(',')
    cpt = 0
    for img in os.listdir(dataPath):
        img_array = cv2.imread(os.path.join(dataPath, img), cv2.IMREAD_GRAYSCALE)
        training_data.append([img_array, labels[cpt]])
        cpt += 1  # Increment the counter to match labels with images

    random.shuffle(training_data)

    x = []
    y = []

    for images, label in training_data:
        x.append(images)
        y.append(label)

    x = np.array(x)
    
    # Encode labels to integers
    label_encoder = LabelEncoder()
    y = label_encoder.fit_transform(y)

    pickle_out = open("../data/images.pickle", "wb")
    pickle.dump(x, pickle_out)
    pickle_out.close()

    pickle_out = open("../data/labels.pickle", "wb")
    pickle.dump(y, pickle_out)
    pickle_out.close()

    # Save the label encoder for future use
    pickle_out = open("../data/label_encoder.pickle", "wb")
    pickle.dump(label_encoder, pickle_out)
    pickle_out.close()

def open_dataset():
    img_train = pickle.load(open("../data/images.pickle", "rb"))
    labels_train = pickle.load(open("../data/labels.pickle", "rb"))
    return img_train, labels_train

def get_image(data):
    img_array = cv2.imread(data, cv2.IMREAD_GRAYSCALE)
    return img_array

def model_build():
    x_train, y_train = open_dataset()
    x_train = np.array(x_train)
    y_train = np.array(y_train)
    
    x_train = tf.keras.utils.normalize(x_train, axis=1)

    x_train = np.expand_dims(x_train, axis=-1)
    
    datagen = ImageDataGenerator(
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True,
        fill_mode='nearest'
    )

    datagen.fit(x_train)

    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.Conv2D(32, (3, 3), activation='relu', input_shape=(1080, 1440, 1)))
    model.add(tf.keras.layers.BatchNormalization())
    model.add(tf.keras.layers.MaxPooling2D((2, 2)))
    model.add(tf.keras.layers.Conv2D(64, (3, 3), activation='relu'))
    model.add(tf.keras.layers.BatchNormalization())
    model.add(tf.keras.layers.MaxPooling2D((2, 2)))
    model.add(tf.keras.layers.Conv2D(128, (3, 3), activation='relu'))
    model.add(tf.keras.layers.BatchNormalization())
    model.add(tf.keras.layers.MaxPooling2D((2, 2)))
    model.add(tf.keras.layers.Conv2D(128, (3, 3), activation='relu'))
    model.add(tf.keras.layers.BatchNormalization())
    model.add(tf.keras.layers.MaxPooling2D((2, 2)))
    model.add(tf.keras.layers.Flatten())
    model.add(tf.keras.layers.Dense(128, activation='relu'))
    model.add(tf.keras.layers.Dropout(0.5))
    model.add(tf.keras.layers.Dense(len(set(y_train)), activation='softmax'))

    model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])

    # model.fit(x_train, y_train, epochs=50)
    early_stopping = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=5, restore_best_weights=True)
    model.fit(x_train, y_train, epochs=50, validation_split=0.2, callbacks=[early_stopping])

    model.save('my_model5.h5')

def model_fit(data):
    model = tf.keras.models.load_model('my_model4.h5')

    x_test = get_image(data)
    x_test = tf.keras.utils.normalize(np.array([x_test]), axis=1)
    
    predictions = model.predict(x_test)
    print(predictions)
    label_encoder = pickle.load(open("../data/label_encoder.pickle", "rb"))
    predicted_label = label_encoder.inverse_transform([np.argmax(predictions)])
    
    return predicted_label[0]

# create_training_data()
model_build()
# print(model_fit("../img/exo273MGE_74932_120_1584960540510.BMP"))
